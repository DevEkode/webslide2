<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['duration','order','anim_param'];

    public function image(){
        return $this->belongsTo('App\Image');
    }

    public function animation(){
        return $this->belongsTo('App\Animation');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }
}
