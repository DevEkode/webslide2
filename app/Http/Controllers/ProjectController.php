<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all user projects
        $projects = \Auth::user()->projects;
        // Return the view with the projects
        return view('projects.index',['projects'=>$projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Load project creation form
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //First validate the values
        $validatedData = $request->validate([
           'projectName' => 'required',
        ]);

        //Create project
        $project = new Project;
        $project->name = $validatedData['projectName'];
        $project->save();

        \Auth::user()->projects()->attach($project);

        return \redirect('projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        $slides = $project->slides()->orderBy('order','asc')->get();

        return view('projects.show',[
            'project' => $project,
            'slides_orm' => $slides,
            'slides' => $slides->toJson()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Get the corresponding project
        $project = auth()->user()->projects->find($id);
        //Send 403 if the user is not the creator of this project
        if($project == null){
            abort(403);
        }

        return view('projects.edit',[
            'project'=>$project,
            'slides'=>$project->slides->sortBy('order')
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $slides = $project->slides();
        $slides->delete();
        $project->delete();

        return \redirect(route('projects.index'));
    }
}
