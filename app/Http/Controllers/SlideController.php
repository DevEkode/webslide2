<?php

namespace App\Http\Controllers;

use App\Animation;
use App\Image;
use App\Project;
use App\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        //Get corresponding project
        $project = Project::find($project_id);
        //Get all project slides
        $slides = $project->slides;

        //TODO : return slides
        abort(405);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id)
    {
        $project = Project::find($project_id);
        $animations = Animation::all();
        return view('slides.create', [
            'project' => $project,
            'animations' => $animations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_project)
    {
        $validatedData = $request->validate([
            'slideImage' => 'required|image|mimes:jpeg,png,jpg',
            'slideDuration' => 'required',
            'animationSelect' => 'required'
        ]);

        $project = Project::find($id_project);
        $image     = new Image;
        $slide     = new Slide;

        if ($request->file('slideImage')) {
            $imagePath = $request->file('slideImage');
            $imageName = $imagePath->getClientOriginalName();

            $path = $request->file('slideImage')->storeAs('uploads', $imageName, 'public');

            //$image->name = $imageName;
            $image->url = '/storage/'.$path;
            $image->save();
            $slide->image()->associate($image);
        }
        $slide->duration = $validatedData['slideDuration'];

        //Animation
        if($validatedData['animationSelect'] != -1){
            $animation = Animation::find($validatedData['animationSelect']);
            $slide->animation()->associate($animation);
        }

        //Set slide order
        $slide->order = count($project->slides);

        //Save slide into project
        $project->slides()->save($slide);

        //Redirect to toute a modifier
        return redirect(route('projects.edit', $id_project));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //TODO : Check if the user is the owner of this slide
        $slide = Slide::find($id);
        $animations = Animation::all();

        //Return edit view
        return view('slides.edit', [
            'slide' => $slide,
            'animations' => $animations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'slideImage' => 'image|mimes:jpeg,png,jpg',
            'slideDuration' => 'required',
            'animationSelect' => 'required'
        ]);

        $slide       = Slide::find($id);
        $image      = $slide->image;

        if ($request->file('slideImage')) {
            $imagePath = $request->file('slideImage');
            $imageName = $imagePath->getClientOriginalName();

            $path = $request->file('slideImage')->storeAs('uploads', $imageName, 'public');

            //$image->name = $imageName;
            $image->url = '/storage/'.$path;
            $image->save();
        }

        if($validatedData['animationSelect'] != -1){
            $anim = Animation::find($validatedData['animationSelect']);
            $slide->animation()->associate($anim);
        }else{
            $slide->animation_id = null;
        }

        $slide->duration = $validatedData['slideDuration'];

        $slide->save();

        $project = $slide->project;
        return redirect(route('projects.edit', $project));
    }

    public function updateOrder(Request $request, $id){
        $validateData = $request->validate([
            'action' => 'required',
        ]);

        $slide = Slide::find($id);
        $project = $slide->project;
        $slides = $project->slides()->orderBy('order','desc')->get();

        $prev_slide = $slides->where('order','<',$slide->order)->max('id');
        $next_slide = $slides->where('order','>',$slide->order)->min('id');

        if($validateData['action'] == 'next'){
            if($next_slide == null) abort(400);

            $temp = Slide::find($next_slide);
            $new_order = $temp->order;
            $temp->order = $slide->order;
            $temp->save();

            $slide->order = $new_order;
            $slide->save();
        }else if($validateData['action'] == 'previous'){
            if($prev_slide == null) abort(400);

            $temp = Slide::find($prev_slide);
            $new_order = $temp->order;
            $temp->order = $slide->order;
            $temp->save();

            $slide->order = $new_order;
            $slide->save();
        }
        return redirect(route('projects.edit',$project));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        $project = $slide->project;
        $slides = $project->slides()->orderBy('order','desc')->get();

        //Re-order slides
        foreach($slides as $s){
            if($s->order > $slide->order){
                $new_order = $s->order;
                $new_order--;
                $s->order = $new_order;
                $s->save();
            }
        }

        $slide->delete();

        return redirect(route('projects.edit',$project));
    }
}
