<?php

namespace App\Http\Controllers;

use App\Animation;
use App\Slide;
use Illuminate\Http\Request;

class AnimationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slide_id
     * @param $anim_id
     * @return void
     */
    public function edit($slide_id,$anim_id)
    {
        //Get slide ORM
        $slide = Slide::find($slide_id);
        $anim = Animation::find($anim_id);
        $array = array(
            'slide' => $slide,
            'animation' => $anim
        );

        //Return corresponding view for this animation
        switch($anim_id){
            case 1: return view('animations.edit.kenburns',$array);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slide_id,$anim_id)
    {
        //Get validation json from database
        $anim = Animation::find($anim_id);
        $validation_json = json_decode($anim->param_model,true);

        $validatedData = $request->validate($validation_json);

        //Save new data to slide
        $slide = Slide::find($slide_id);
        $slide->anim_param = $validatedData;
        $slide->save();

        return redirect(route('slides.edit',[$slide]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
