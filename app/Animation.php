<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animation extends Model
{
    protected $fillable = ['name','param_model'];
}
