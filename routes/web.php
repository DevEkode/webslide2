<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/slides/{slide}/updateorder','SlideController@updateOrder')->name('slides.updateorder');


Route::resource('projects','ProjectController');
Route::resource('projects.slides','SlideController')->shallow();
Route::resource('slides.animation','AnimationController');
