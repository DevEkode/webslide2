<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->float('duration');
            $table->smallInteger('order')->default(-1);
            $table->text('anim_param')->nullable();

            $table->unsignedBigInteger('project_id')->nullable();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->unsignedBigInteger('animation_id')->nullable();
            $table->foreign('animation_id')->references('id')->on('animations');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(DB::getDriverName() !== 'sqlite'){
            Schema::table('slides',function(Blueprint $table){
                $table->dropForeign('project_id');
                $table->dropForeign('animation_id');
                $table->dropForeign('image_id');
            });
        }

        Schema::dropIfExists('slides');
    }
}
