<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $animation = factory(App\Animation::class)->create();

        $users = factory(App\User::class,3)->create()->each(function($user) use ($animation){
            $project = factory(App\Project::class)->create();

            $slides = factory(App\Slide::class,3)->create()->each(function ($slide) use ($animation){
                $slide->image()->associate(factory(App\Image::class)->create());
                $slide->animation()->associate($animation);
            });
            $project->slides()->saveMany($slides);

            $user->projects()->attach($project);
        });
    }
}
