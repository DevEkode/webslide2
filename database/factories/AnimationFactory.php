<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Animation::class, function (Faker $faker) {
    $json = array(
        'startScale' => 'required|numeric|between:0,1',
        'endScale' => 'required|numeric|between:0,1',
        'startX' => 'required|numeric',
        'startY' => 'required|numeric',
        'endX' => 'required|numeric',
        'endY' => 'required|numeric'
    );

    return [
        'name' => 'Ken Burns',
        'param_model' => json_encode($json)
    ];
});
