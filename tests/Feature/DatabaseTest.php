<?php

namespace Tests\Feature;

use App\Project;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DatabaseTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCreation(){
        $user = factory(User::class)->create();

        $this->assertDatabaseHas('users',['id' => 1]);
    }

    public function testUsersCreation(){
        $users = factory(User::class,2)->create();

        $this->assertDatabaseHas('users',['id' => 1]);
        $this->assertDatabaseHas('users',['id' => 2]);
    }

    public function testProjectCreation(){
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();

        $user->projects()->attach($project);

        $this->assertDatabaseHas('projects',[
            'id' => $project->id,
            'name' => 'default projects'
        ]);
        $this->assertDatabaseHas('user_project',[
           'project_id' => $project->id,
            'user_id' => $user->id
        ]);
    }

    public function testProjectDelete(){
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();
        $user->projects()->attach($project);

        $project->delete();

        $this->assertDatabaseMissing('projects',[
            'id' => $project->id,
            'name' => 'default projects'
        ]);
    }
}
