let width = window.innerWidth;
let height = window.innerHeight;

let json_slides = JSON.parse(slides);
let canvas = document.getElementById('canvas')
let slide_nb = document.getElementById('slide_nb');

let slideIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > x.length) {
        slideIndex = 1
    }

    let current_slide = x[slideIndex - 1];
    //Get animation and duration
    let slide_obj;
    json_slides.forEach(function(slide){
        if(slide.id === parseInt(current_slide.id)) slide_obj = slide;
    })

    let img = new Image();
    img.onload = function(){
        let img_size = calculateAspectRatioFit(this.width,this.height,canvas.clientWidth,height);
        current_slide.style.width = img_size.width+'px';
        current_slide.style.height = img_size.height+'px';
        current_slide.style.display = "block";
        slide_nb.innerHTML = slideIndex+"/"+json_slides.length;
        setTimeout(carousel, slide_obj.duration*1000); // Change image every 2 seconds
    }
    img.src = current_slide.src;


}

function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return {width: srcWidth * ratio, height: srcHeight * ratio,ratio: ratio};
}



