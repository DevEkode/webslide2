/* Specific JS to handle ken-burns effect editor */
let container = document.getElementById('canvas');
let max_height = 500;

window.onload = function () {
    let stage = new Konva.Stage({
        container: 'canvas',
        width: container.clientWidth,
        height: max_height
    });

    //Add image layer in the background
    createImageLayer(image.url, container.clientWidth, max_height, function (img_layer, img_size) {
        stage.add(img_layer);

        let start_sel = createNewSelectionRectangleLayer(container.clientWidth, max_height, img_size.width, img_size.height,1,'Start');
        let end_sel = createNewSelectionRectangleLayer(container.clientWidth, max_height, img_size.width, img_size.height,0.5,'End');

        let margins = calculateMargin(container,img_size);
        if(anim_param != null){
            //The animation has saved parameters, load
            let json_params = JSON.parse(anim_param);
            let startScale = parseFloat(json_params.startScale);
            let endScale = parseFloat(json_params.endScale);
            let startX = parseFloat(json_params.startX);
            let startY = parseFloat(json_params.startY);
            let endX = parseFloat(json_params.endX);
            let endY = parseFloat(json_params.endY);

            start_sel = createSelectionRectangleLayer(startX, startY, img_size.width, img_size.height,startScale,'Start');
            end_sel = createSelectionRectangleLayer(endX, endY, img_size.width, img_size.height,endScale,'End');
        }
        stage.add(start_sel.layer);
        stage.add(end_sel.layer);

        //Add update events
        start_sel.rect.on('dragmove',function(){
            updateForm(start_sel.rect,end_sel.rect,img_size.ratio,margins);
        });
        end_sel.rect.on('dragmove',function(){
            updateForm(start_sel.rect,end_sel.rect,img_size.ratio,margins);
        });
        start_sel.rect.on('transform',function(){
            updateForm(start_sel.rect,end_sel.rect,img_size.ratio,margins);
        });
        end_sel.rect.on('transform',function(){
            updateForm(start_sel.rect,end_sel.rect,img_size.ratio,margins);
        });
        updateForm(start_sel.rect,end_sel.rect,img_size.ratio,margins);
    });
    //Add selection rectangles
}

function calculateMargin(container,img_size){
    let horizontal_margin = (container.clientWidth - img_size.width)/2;
    let vertical_margin = (container.clientHeight - img_size.height)/2;
    return {left: horizontal_margin, top:vertical_margin};
}

function updateForm(start,end,ratio,margin){
    let startScale_input = document.getElementById('startScale');
    let endScale_input = document.getElementById('endScale');
    let startX_input = document.getElementById('startX');
    let startY_input = document.getElementById('startY');
    let endX_input = document.getElementById('endX');
    let endY_input = document.getElementById('endY');

    startScale_input.value = (start.scaleX());
    endScale_input.value = (end.scaleY());
    startX_input.value = calculateValues(start.x(),ratio,margin.left);
    startY_input.value = calculateValues(start.y(),ratio,margin.top);
    endX_input.value = calculateValues(end.x(),ratio,margin.left);
    endY_input.value = calculateValues(end.y(),ratio,margin.top);
}

function calculateValues(value,ratio,margin){
    return ((value/ratio)-margin/ratio);
}

function unCalculateValues(value,ratio,margin){
    return (value*ratio)+margin*ratio;
}

function createNewSelectionRectangleLayer(max_width, max_height, width, height,scale,text){
    let x = max_width / 2;
    let y = max_height / 2;

    return createSelectionRectangleLayer(x,y,width,height,scale,text);
}

function createSelectionRectangleLayer(x, y, width, height,scale,text) {
    let start_rect_layer = new Konva.Layer();
    let rect = new Konva.Rect({
        height: height,
        width: width,
        offsetX: width / 2,
        offsetY: height / 2,
        x: x,
        y: y,
        scaleX : scale,
        scaleY: scale,
        stroke: 'black',
        strokeWidth: 1,
        draggable: true
    });
    start_rect_layer.add(rect);

    let point = new Konva.Circle({
        radius: 5,
        x: x,
        y: y,
        stroke: 'black',
        strokeWidth: 1,
    });
    start_rect_layer.add(point);

    let sel_text = new Konva.Text({
        x: x,
        y: y,
        offsetX: width / 2-10,
        offsetY: height / 2-10,
        text: text,
        fontSize: 30/scale,
        scaleX : scale,
        scaleY: scale,
        fontFamily: 'Calibri'
    });
    start_rect_layer.add(sel_text);

    let trans = new Konva.Transformer({
        nodes: [rect,point,sel_text],
        keepRatio: true,
        rotateEnabled: false,
        enabledAnchors: [
            'top-left',
            'top-right',
            'bottom-left',
            'bottom-right',
        ],
        boundBoxFunc: function(oldBoundBox,newBoundBox){
            if(Math.abs(newBoundBox.width) > width){
                return oldBoundBox;
            }
            return newBoundBox;
        }
    });

    start_rect_layer.add(trans);

    return {layer: start_rect_layer,rect: rect};
}

function createImageLayer(src, max_width, max_height, callback) {
    //Load image and get size
    let img = new Image();
    img.onload = function () {
        let img_size = calculateAspectRatioFit(this.width, this.height, max_width, max_height)

        let img_layer = new Konva.Layer();

        let imageObj = new Image();
        imageObj.onload = function () {
            let image = new Konva.Image({
                image: imageObj,
                height: img_size.height,
                width: img_size.width,
                offsetX: img_size.width / 2,
                offsetY: img_size.height / 2,
                x: max_width / 2,
                y: max_height / 2
            });
            img_layer.add(image);
            img_layer.batchDraw();
            callback(img_layer, img_size);
        }
        imageObj.src = src;
    }
    img.src = src;
}

function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return {width: srcWidth * ratio, height: srcHeight * ratio,ratio: ratio};
}



