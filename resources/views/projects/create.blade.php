@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">Create your new project</h1>

        <form method="POST" action="{{ url('projects') }}">
            @csrf
            <div class="form-group">
                <label for="projectName">Project name</label>
                <input type="text" name="projectName" id="projectName" aria-describedby="projectNameHelp" class="form-control @error('projectName') is-invalid @enderror">
                <small id="projectNameHelp" class="form-text text-muted">Choose a good name</small>
                @error('projectName')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Create</button>
            <a href="{{url()->previous()}}" class="btn btn-danger">Cancel</a>
        </form>
    </div>

@endsection
