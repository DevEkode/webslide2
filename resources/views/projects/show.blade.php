@extends('layouts.app')

@section('js')
    <script>
        let slides = {!! json_encode($slides) !!}
    </script>
    <script src="{{ asset('js/preview.js') }}" defer></script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center mb-5">
            <a href="{{route('projects.edit',$project)}}" class="btn btn-outline-primary">Exit preview</a>
        </div>
        <div class="row justify-content-center mb-5">
            <h1 class="m-0 mr-3">Slide <span id="slide_nb"></span></h1>
        </div>
        <div id="canvas" class="text-center">
            @foreach($slides_orm as $slide)
                <img src="{{url($slide->image->url)}}" id="{{$slide->id}}" class="mySlides">
            @endforeach
        </div>



    </div>
@endsection
