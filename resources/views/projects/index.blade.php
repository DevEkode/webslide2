@extends('layouts.app')

@section('content')
    <div class="row justify-content-center mb-5">
        <h1 class="m-0 mr-3">Projects</h1> <a href="{{route('projects.create')}}" class="btn btn-outline-primary">+</a>
    </div>
    <div class="row justify-content-center">
        <!-- Display user projects -->
        @foreach($projects as $project)
            <div class="card" style="width: 18rem;">
                @if (count($project->slides) > 1)
                    <img src="{{$project->slides[0]->image->url}}" class="card-img-bottom" alt="slide image">
                @endif
                <div class="card-body">
                    <h5 class="card-title">{{$project->name}}</h5>
                    <a href="{{ url('projects/'.$project->id.'/edit') }}" class="btn btn-primary">Open</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
