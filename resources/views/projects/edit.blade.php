@extends('layouts.app')

@section('content')
    <div class="row justify-content-center mb-5">
        <h1 class="m-0 mr-3">{{ $project->name }}</h1>
        <form method="POST" action="{{route('projects.destroy',$project)}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Delete</button>
        </form>
    </div>

    <div class="row justify-content-center mb-5">
        <a href="{{route('projects.show',$project)}}" class="btn btn-outline-primary">Go to preview</a>
    </div>

    <div class="row justify-content-center mb-5">
        <h1 class="m-0 mr-3">Slides</h1> <a href="{{route('projects.slides.create',$project)}}" class="btn btn-outline-primary">+</a>
    </div>
    <div class="justify-content-center d-flex pl-5" style="overflow-x: scroll;">
        @if($slides->isEmpty())
            <h4 class="text-center">Create your first slide with the + button !</h4>
        @endif
        @foreach($slides as $slide)
            <div class="card mr-1" style="width: 18rem; min-width: 18rem">
                <img class="card-img-top" src="{{url($slide->image->url)}}">
                <div class="card-body">
                    <a href="{{url('slides/'.$slide->id.'/edit')}}" class="btn btn-primary">Edit</a>
                    <a href="{{route('slides.updateorder',[$slide,'action'=>'next'])}}"
                       class="btn btn-light float-right {{$slide->order == count($slides)-1 ? 'disabled':''}}">Next</a>
                    <a href="{{route('slides.updateorder',[$slide,'action'=>'previous'])}}"
                       class="btn btn-light float-right {{$slide->order == 0 ? 'disabled':''}}">Previous</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
