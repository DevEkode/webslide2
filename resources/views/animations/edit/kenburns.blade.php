@extends('layouts.app')

@section('js')
    <script>
        let image = {!! json_encode($slide->image) !!};
        let anim_param = {!! json_encode($slide->anim_param) !!};
    </script>
    <script src="{{ asset('js/kenburns.js') }}" defer></script>
@endsection

@section('content')
    <div class="container">
        <!-- Effect preview row -->
        <div class="row">
            <div class="w-100 h-100" id="canvas"></div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif
        <!-- Parameter row -->
        <div class="row">
            <form class="w-100" method="POST" action="{{route('slides.animation.update',[$slide,$slide->animation])}}">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="startScale">Start scale</label>
                            <input type="text" id="startScale" name="startScale" class="form-control" placeholder="Start scale">
                            @error('startScale')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="endScale">End scale</label>
                            <input type="text" id="endScale" name="endScale" class="form-control" placeholder="End scale">
                            @error('endScale')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="startX">Start X position</label>
                            <input type="text" id="startX" name="startX" class="form-control" placeholder="Start X">
                            @error('startX')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="startY">Start Y position</label>
                            <input type="text" id="startY" name="startY" class="form-control" placeholder="Start Y">
                            @error('startY')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="endX">End X position</label>
                            <input type="text" id="endX" name="endX" class="form-control" placeholder="End X">
                            @error('endX')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="endY">End Y position</label>
                            <input type="text" id="endY" name="endY" class="form-control" placeholder="End Y">
                            @error('endY')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
                <a href="{{route('slides.edit',$slide)}}" class="btn btn-danger">Cancel</a>
            </form>
        </div>
    </div>
@endsection
