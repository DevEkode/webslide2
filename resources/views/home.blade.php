@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <h1 class="text-center">Projects </h1>
    <h2 class="text-center"> <a href="{{ url('projects/create') }}" class="btn btn-success">Create new Project</a></h2>
    <div class="row justify-content-center">
        <!-- Display user projects -->
        @foreach($projects as $project)
            <div class="card" style="width: 18rem;">
                @if (count($project->slides) > 1)
                    <img src="{{$project->slides[0]->image->url}}" class="card-img-bottom" alt="slide image">
                @endif
                <div class="card-body">
                    <h5 class="card-title">{{$project->name}}</h5>
                    <a href="{{ url('projects/'.$project->id.'/edit') }}" class="btn btn-primary">Open</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
