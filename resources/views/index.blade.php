@extends('layouts.app')

@section('content')
    <div class="container text-center">
        <h1 class="display-1">Webslide²</h1>
        <h3 class="mb-5">Online simple slideshow creator</h3>

        <div class="card text-center">
            <div class="card-body">
                @guest
                    <h5 class="card-title"><b>Register or login to create your first project</b></h5>
                    <a href="{{url('login')}}" class="btn btn-primary">Login</a>
                    <a href="{{url('register')}}" class="btn btn-primary">Register</a>
                @endguest
                @auth
                        <h5 class="card-title"><b>Still logged ? Your projects are waiting</b></h5>
                        <a href="{{url('projects')}}" class="btn btn-primary">Go to projects</a>
                @endauth
            </div>
        </div>

    </div><br><br><br>

    <div class="container">

        <div class="row featurette">
            <div class="col-md-12">
                <h2 class="featurette-heading">Use Webslide² <span class="text-muted"> the slide creator</span></h2>
                <p class="lead">is designed to generate animation on images. A web application to give effects to your
                    souvenir photos. Originally on a version of SYMFONY, a new version made this time on LARAVEL.</p>
                <p>Originating from the project: Alexis POUPELIN & Quentin DOPPLER.</p>
            </div>

        </div>

        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{asset('storage/images/undraw_building_blocks_n0nc.png')}}"
                     alt="Generic placeholder image">
            </div>
            <div class="col-md-7">
                <h2 class="featurette-heading">Create projects <span
                        class="text-muted"> to have crazy experiences</span></h2>
                <p class="lead">Project configuration management for all types of users. Each account can experiment
                    without difficulty.</p>
            </div>
        </div>

        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Generate your slides</h2>
                <p class="lead">to allow you to scroll through your images. Configure your slides and animations to make
                    rich and dynamic presentations</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{asset('storage/images/undraw_animating_1rgh.png')}}"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{asset('storage/images/undraw_pair_programming_njlp.png')}}"
                     alt="Generic placeholder image">
            </div>
            <div class="col-md-7">
                <h2 class="featurette-heading">Webslide² <span class="text-muted"> Your Favorite editor.</span></h2>
                <p class="lead">Webslide² is a suite of modern presentation tools, available right from your browser.
                    Unlike traditional presentation software, there's no need to download anything. Working with
                    collaborators to make an awe-inspiring presentation has never been easier.</p>
            </div>
        </div>

        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Join <span class="text-muted"> ,200 creators.</span></h2>
                <p class="lead">Webslide² is used daily by professionals all over the world to speak at conferences,
                    share pitches, school work, portfolios and so much more. Join today and try it out for free.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{asset('storage/images/undraw_status_update_jjgk.png')}}"
                     alt="Generic placeholder image">
            </div>
        </div>

    </div>

@endsection
