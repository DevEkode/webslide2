@extends('layouts.app')

@section('js')
    <script>
        window.onload = function(){
            //Set event
            document.getElementById('slideImage').addEventListener("change",function(event){
                loadFile(event);
            });
            let loadFile = function(event) {
                let preview = document.getElementById('preview');
                let label = document.getElementById('slideImageLabel')
                //Load image into preview
                preview.src = URL.createObjectURL(event.target.files[0]);
                label.innerHTML = event.target.files[0].name;
                preview.onload = function() {
                    URL.revokeObjectURL(preview.src) // free memory
                }
            };
        }
    </script>
@endsection

@section('css')

@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">Edit this slide</h1>

        <form method="POST" action="{{ route('slides.update',$slide) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-group">
                <img src="{{($slide->image->url)}}" id="preview" alt="image upload preview" class="img-fluid mb-2">
                <div class="custom-file mb-3">
                    <label class="custom-file-label" for="slideImage" id="slideImageLabel">{{$slide->image->url}}</label>
                    <input type="file" accept="image/*" name="slideImage" id="slideImage"
                           class="custom-file-input @error('slideImage') is-invalid @enderror">
                    @error('slideImage')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input type="text" id="slideDuration" name="slideDuration" value="{{$slide->duration}}"
                           class="form-control @error('slideDuration') is-invalid @enderror"
                           placeholder="Enter slide duration (in seconds)" aria-describedby="basic-addon1">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon1">s</span>
                    </div>
                    @error('slideDuration')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <label for="animationSelect"><b>(optional)</b> Set an animation for this slide</label>
                <div class="form-row align-items-center">
                    <div class="col">

                        <select class="form-control @error('animationSelect') is-invalid @enderror" id="animationSelect"
                                name="animationSelect" autocomplete="off">
                            @if($slide->animation == null)
                                <option value="-1" selected="selected">none</option>
                                @foreach($animations as $animation)
                                    <option
                                        value="{{$animation->id}}">{{ $animation->name }}</option>
                                @endforeach
                            @else
                                <option value="-1">none</option>
                                @foreach($animations as $animation)
                                    <option
                                        value="{{$animation->id}}" {{ $animation->id == $slide->animation->id ? 'selected':'' }}>{{ $animation->name }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                    <div class="col">
                        @if($slide->animation == null)
                            <a href="" class="btn btn-primary disabled">Edit animation</a>
                        @else
                            <a href="{{route('slides.animation.edit',[$slide,$slide->animation])}}" class="btn btn-primary">Edit animation</a>
                        @endif
                    </div>

                    @error('animationSelect')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

            </div>

            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{url('projects/'.$slide->project->id.'/edit')}}" class="btn btn-danger">Cancel</a>
            <form method="POST" action="{{route('slides.destroy',$slide)}}">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger float-right">Delete this slide</button>
            </form>

        </form>
    </div>
@endsection
