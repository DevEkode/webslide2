@extends('layouts.app')

@section('js')
    <script>
        //Set event
        window.onload = function(){
            document.getElementById('slideImage').addEventListener("change",function(event){
                loadFile(event);
            });
            let loadFile = function(event) {
                let preview = document.getElementById('preview');
                let label = document.getElementById('slideImageLabel')
                //Load image into preview
                preview.src = URL.createObjectURL(event.target.files[0]);
                label.innerHTML = event.target.files[0].name;
                preview.onload = function() {
                    URL.revokeObjectURL(preview.src) // free memory
                }
            };
        }

    </script>
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">Create your new slide</h1>

        <form method="POST" action="{{ route('projects.slides.store',$project) }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <img src="" id="preview" alt="image upload preview" class="img-fluid mb-2">
                <div class="custom-file mb-3">
                    <label class="custom-file-label" for="slideImage" id="slideImageLabel">Choose image file</label>
                    <input type="file" accept="image/*" name="slideImage" id="slideImage"
                           class="custom-file-input @error('slideImage') is-invalid @enderror">
                    @error('slideImage')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input type="text" id="slideDuration" name="slideDuration" class="form-control @error('slideDuration') is-invalid @enderror" placeholder="Enter slide duration (in seconds)" aria-describedby="basic-addon1">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon1">s</span>
                    </div>
                    @error('slideDuration')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="animationSelect"><b>(optional)</b> Set an animation for this slide</label>
                    <select class="form-control @error('animationSelect') is-invalid @enderror" id="animationSelect" name="animationSelect">
                        @foreach($animations as $animation)
                            <option value="-1" selected>none</option>
                            <option value="{{$animation->id}}">{{ $animation->name }}</option>
                        @endforeach
                    </select>
                    <small id="animationSelectHelp" class="form-text text-muted">You can access the animation edit menu after the slide creation</small>
                    @error('animationSelect')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

            </div>

            <button type="submit" class="btn btn-success">Create</button>
            <a href="{{url()->previous()}}" class="btn btn-danger">Cancel</a>
        </form>
    </div>
@endsection
