# Webslide²

Webslide² is a webapp to build a simple version of iMovie.  
The first version was developed in Symfony. See more info in the wiki of the GitLab.

## Authors
*  Alexis POUPELIN
*  Quentin DOPPLER


## Requirements
*  PHP >= 7.4
*  composer ([website](https://getcomposer.org/))
*  npm ([website](https://nodejs.org/en/download/))


## Install

### Git clone
1.  `git clone https://gitlab.com/DevEkode/webslide2.git`
2.  `cd webslide2`


## Dependencies install
1.  `composer install`
2.  `npm install`


## Configuration
1.  Copy the `.env.example` file to `.env`.
2.  Edit the  `DB_*` variables with your database settings.
3.  Generate key with : `php artisan key:generate`


## Database migration
1.  Create your DATABASE NAME
2.  Generate the database with `php artisan migrate`
3.  _(Optional)_ To generate some default values into the database, enter : `php artisan db:seed`

## Generate assets
1. Compile js with : `npm run dev`
2. Create storage links with `php artisan storage:link`


## Start server
The project is now installed,
you can start the laravel server with `php artisan serve`.

Enjoy !


