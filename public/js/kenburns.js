/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/kenburns.js":
/*!**********************************!*\
  !*** ./resources/js/kenburns.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* Specific JS to handle ken-burns effect editor */
var container = document.getElementById('canvas');
var max_height = 500;

window.onload = function () {
  var stage = new Konva.Stage({
    container: 'canvas',
    width: container.clientWidth,
    height: max_height
  }); //Add image layer in the background

  createImageLayer(image.url, container.clientWidth, max_height, function (img_layer, img_size) {
    stage.add(img_layer);
    var start_sel = createNewSelectionRectangleLayer(container.clientWidth, max_height, img_size.width, img_size.height, 1, 'Start');
    var end_sel = createNewSelectionRectangleLayer(container.clientWidth, max_height, img_size.width, img_size.height, 0.5, 'End');
    var margins = calculateMargin(container, img_size);

    if (anim_param != null) {
      //The animation has saved parameters, load
      var json_params = JSON.parse(anim_param);
      var startScale = parseFloat(json_params.startScale);
      var endScale = parseFloat(json_params.endScale);
      var startX = parseFloat(json_params.startX);
      var startY = parseFloat(json_params.startY);
      var endX = parseFloat(json_params.endX);
      var endY = parseFloat(json_params.endY);
      start_sel = createSelectionRectangleLayer(startX, startY, img_size.width, img_size.height, startScale, 'Start');
      end_sel = createSelectionRectangleLayer(endX, endY, img_size.width, img_size.height, endScale, 'End');
    }

    stage.add(start_sel.layer);
    stage.add(end_sel.layer); //Add update events

    start_sel.rect.on('dragmove', function () {
      updateForm(start_sel.rect, end_sel.rect, img_size.ratio, margins);
    });
    end_sel.rect.on('dragmove', function () {
      updateForm(start_sel.rect, end_sel.rect, img_size.ratio, margins);
    });
    start_sel.rect.on('transform', function () {
      updateForm(start_sel.rect, end_sel.rect, img_size.ratio, margins);
    });
    end_sel.rect.on('transform', function () {
      updateForm(start_sel.rect, end_sel.rect, img_size.ratio, margins);
    });
    updateForm(start_sel.rect, end_sel.rect, img_size.ratio, margins);
  }); //Add selection rectangles
};

function calculateMargin(container, img_size) {
  var horizontal_margin = (container.clientWidth - img_size.width) / 2;
  var vertical_margin = (container.clientHeight - img_size.height) / 2;
  return {
    left: horizontal_margin,
    top: vertical_margin
  };
}

function updateForm(start, end, ratio, margin) {
  var startScale_input = document.getElementById('startScale');
  var endScale_input = document.getElementById('endScale');
  var startX_input = document.getElementById('startX');
  var startY_input = document.getElementById('startY');
  var endX_input = document.getElementById('endX');
  var endY_input = document.getElementById('endY');
  startScale_input.value = start.scaleX();
  endScale_input.value = end.scaleY();
  startX_input.value = calculateValues(start.x(), ratio, margin.left);
  startY_input.value = calculateValues(start.y(), ratio, margin.top);
  endX_input.value = calculateValues(end.x(), ratio, margin.left);
  endY_input.value = calculateValues(end.y(), ratio, margin.top);
}

function calculateValues(value, ratio, margin) {
  return value / ratio - margin / ratio;
}

function unCalculateValues(value, ratio, margin) {
  return value * ratio + margin * ratio;
}

function createNewSelectionRectangleLayer(max_width, max_height, width, height, scale, text) {
  var x = max_width / 2;
  var y = max_height / 2;
  return createSelectionRectangleLayer(x, y, width, height, scale, text);
}

function createSelectionRectangleLayer(x, y, width, height, scale, text) {
  var start_rect_layer = new Konva.Layer();
  var rect = new Konva.Rect({
    height: height,
    width: width,
    offsetX: width / 2,
    offsetY: height / 2,
    x: x,
    y: y,
    scaleX: scale,
    scaleY: scale,
    stroke: 'black',
    strokeWidth: 1,
    draggable: true
  });
  start_rect_layer.add(rect);
  var point = new Konva.Circle({
    radius: 5,
    x: x,
    y: y,
    stroke: 'black',
    strokeWidth: 1
  });
  start_rect_layer.add(point);
  var sel_text = new Konva.Text({
    x: x,
    y: y,
    offsetX: width / 2 - 10,
    offsetY: height / 2 - 10,
    text: text,
    fontSize: 30 / scale,
    scaleX: scale,
    scaleY: scale,
    fontFamily: 'Calibri'
  });
  start_rect_layer.add(sel_text);
  var trans = new Konva.Transformer({
    nodes: [rect, point, sel_text],
    keepRatio: true,
    rotateEnabled: false,
    enabledAnchors: ['top-left', 'top-right', 'bottom-left', 'bottom-right'],
    boundBoxFunc: function boundBoxFunc(oldBoundBox, newBoundBox) {
      if (Math.abs(newBoundBox.width) > width) {
        return oldBoundBox;
      }

      return newBoundBox;
    }
  });
  start_rect_layer.add(trans);
  return {
    layer: start_rect_layer,
    rect: rect
  };
}

function createImageLayer(src, max_width, max_height, callback) {
  //Load image and get size
  var img = new Image();

  img.onload = function () {
    var img_size = calculateAspectRatioFit(this.width, this.height, max_width, max_height);
    var img_layer = new Konva.Layer();
    var imageObj = new Image();

    imageObj.onload = function () {
      var image = new Konva.Image({
        image: imageObj,
        height: img_size.height,
        width: img_size.width,
        offsetX: img_size.width / 2,
        offsetY: img_size.height / 2,
        x: max_width / 2,
        y: max_height / 2
      });
      img_layer.add(image);
      img_layer.batchDraw();
      callback(img_layer, img_size);
    };

    imageObj.src = src;
  };

  img.src = src;
}

function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
  var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
  return {
    width: srcWidth * ratio,
    height: srcHeight * ratio,
    ratio: ratio
  };
}

/***/ }),

/***/ 1:
/*!****************************************!*\
  !*** multi ./resources/js/kenburns.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\webslide2\resources\js\kenburns.js */"./resources/js/kenburns.js");


/***/ })

/******/ });